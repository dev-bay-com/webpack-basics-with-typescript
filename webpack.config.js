const HtmlWebpackPlugin = require('html-webpack-plugin');
const { CleanWebpackPlugin } = require('clean-webpack-plugin');
const path = require('path');

module.exports = {
  mode: 'development',
  devtool: 'inline-source-map',
  devServer: {
    contentBase: './dist'
  },
  entry: './src/app.ss',
  //entry: './src/appTS.ts',
  output: {
    path: path.resolve(__dirname, './dist'),
    filename: 'bundle.js'
  },
  plugins: [
    new HtmlWebpackPlugin({
      title: 'Optional chaining - test'
    }),
    new CleanWebpackPlugin()
  ],
  module: {
    rules: [{
      test: /\.js$/,
      exclude: /node_modules/,
      use: {
        loader: 'babel-loader',
        options: {
          presets: ['@babel/preset-env']
        }
      }
    }, {
      test: /\.ts$/,
      //include: /\appBabel.ts$/, //remove or comment to work with optional chaining in TS
      exclude: /node_modules/,
      use: {
        loader: 'babel-loader',
        options: {
          presets: ['@babel/preset-typescript'],
          plugins: ["@babel/plugin-proposal-optional-chaining"]
        }
      }
    },
		// TS-LOADER
		// {
      // test: /\.ts$/,
      // //include: /\appTsLoader.ts$/, //remove or comment to work with optional chaining in TS
      // exclude: /node_modules/,
      // use: 'ts-loader',
      // exclude: /node_modules/
    // }
		]
  }
}