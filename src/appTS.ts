type Space = {
  desk?: string[],
  table?: string[],
  walls?: string[]
};

type Building = {
  room?: Space,
  toilet?: Space,
  Kitchen?: Space
};

const house: Building = {
  room: {
    desk: ['pencil', 'computer']
  }
}

const x = house.room.desk;
const y = house.room?.desk;
const z = house.toilet?.table;

export const run = () => {
  document.querySelector('body').innerHTML += `
    <style>
      .text {
        width: 500px;
        margin: 0 auto;
        background: #ccc;
      }
    </style>
    <div class="text">
      <h2>BABEL TYPESCRIPT</h2>
      Room Normal: ${x} <br>
      Room Chaining: ${y} <br>
      Toilet Chaining: ${z}      
    </div>
  `;
};

run();