const house = {
  room: {
    desk: ['pencil', 'computer']
  }
}

const x = house.room.desk;
const y = house.room?.desk;
const z = house.toilet?.closet;

document.querySelector('body').innerHTML += `
  <style>
    .text {
      width: 500px;
      margin: 0 auto;
      background: #ccc;
    }
  </style>
  <div class="text">
    <h2>BABEL PRESET-ENV</h2>
    Room Normal: ${x} <br>
    Room Chaining: ${y} <br>
    Toilet Chaining: ${z}
  </div>
`;